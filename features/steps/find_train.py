from gettext import find
from lib2to3.pgen2 import driver
import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from behave import given, when, then

@when("I search for '{trainnr}'")
def search_train(context, trainnr):
    search_by_trainnr = context.browser.find_element_by_css_selector("input[ref='searchbytrainbutton']")
    searchbar = context.browser.find_element_by_css_selector("input[name='search']")

    search_by_trainnr.click()
    searchbar.clear()
    searchbar.send_keys(context.data["trainnr"])
    time.sleep(3)

@then("Find the train")
def something(context):
    print()