from gettext import find
from lib2to3.pgen2 import driver
import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from behave import given, when, then

@when("I login using '{credentials}'")
def use_creds(context, credentials):
    email = context.browser.find_element_by_css_selector("input#input1")
    password = context.browser.find_element_by_css_selector("input#input2")
    login_btn=context.browser.find_element_by_css_selector("form#loginForm button.btn")

    email.clear()
    password.clear()
    email.send_keys(context.data["creds"]["email"])
    password.send_keys(context.data["creds"]["password"])
    print()
    login_btn.click()
    print()

@then("I should be logged in")
def todo(context):
    logged_in_message=context.browser.find_element_by_css_selector("div.c-header__subtitle").text
    time.sleep(5)
    assert logged_in_message=="You are logged in with immediate access to the online services of your My SNCB account", print("Logged in message not found\r\nfound:"+str(logged_in_message))
    print()