# first we import the needed modules. in this case we are importing webdriver from the selenium module.
# And we are also importing ChromedriverManager from the webdriver_manager module.
# webdriver_manager is a plugin that will take care of downloading our webdriver(s) and it will also add the location of the respective drivers to our
# environment variables.

from distutils.log import error
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from behave import given, when, then

#here we define our 'Given' step. we initialize our driver to run our tests in the crome brower. 
@given("I open my chrome browser")
def initialize_chrome_driver(context):
    context.browser = webdriver.Chrome(ChromeDriverManager().install())

#here we instruct the browser to navigate to 'google.com'
@when("I navigate to '{homepage}'")
def navigate_to_web(context, homepage):
    context.browser.get(homepage)

    print()

#Here we define our 'Then' step. we are looking  wether the title eguals to 'Google'
@then("I should be on '{homepage}'")
def assert_get_correct_title(context, homepage):
    assert context.browser.current_url == homepage, print("expected URL: "+ homepage+"\r\ncurrent URL: "+ context.browser.current_url)
