import json

#set baseURL
def before_all(context):
    context.data = {}
    with open("mappings.json") as dataFile:
        context.data = json.loads(dataFile.read())


def after_all(context):
    True