Feature: Navigate to websites
       
Scenario Outline: navigate to websites
    Given I open my chrome browser
    When I navigate to '<homepage>'
    Then I should be on '<homepage>'

Examples:
    | homepage |
    | https://www.belgiantrain.be/nl/  |

