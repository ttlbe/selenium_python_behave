Feature: Log-in to NMBS

Scenario Outline: Log-in
    Given I open my chrome browser
    When I navigate to '<homepage>'
    And I login using '<credentials>'
    Then I should be logged in

Examples:
    | credentials | homepage |
    | creds | https://www.belgiantrain.be/en/my-account |