Feature: Find train

Scenario Outline: Find train on train map nmbs
    Given I open my chrome browser
    When I navigate to '<homepage>'
    And I search for '<trainnr>'
    Then Find the train

Examples:
    |trainnr|homepage|
    |trainnr|https://trainmap.belgiantrain.be/|
